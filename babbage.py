import re, itertools, vigenere
from string import ascii_uppercase

ALPHABET = ascii_uppercase
ALPHABET_SIZE = len(ALPHABET)

sym_num_alphabet={}
num_sym_alphabet={}

for idx in range(ALPHABET_SIZE):
    key = ALPHABET[idx]
    sym_num_alphabet[key] = idx
    num_sym_alphabet[idx] = key

letter_frequency_distribution = {
    "E": 0.111607,
    "A": 0.084966,
    "R": 0.075809,
    "I": 0.075448,
    "O": 0.071635,
    "T": 0.069509,
    "N": 0.066544,
    "S": 0.057351,
    "L": 0.054893,
    "C": 0.045388,
    "U": 0.036308,
    "D": 0.033844,
    "P": 0.031671,
    "M": 0.030129,
    "H": 0.030034,
    "G": 0.024705,
    "B": 0.020720,
    "F": 0.018121,
    "Y": 0.017779,
    "W": 0.012899,
    "K": 0.011016,
    "V": 0.010074,
    "X": 0.002902,
    "Z": 0.002722,
    "J": 0.001965,
    "Q": 0.001962
}


def babbage_attack(path, check_word):
    appearance_indexes = dict()
    file = open(path, "r")
    content = file.read()
    content = re.sub("[^A-Za-z]", "", content)
    out_file = open("./kasiski.txt", "w")
    all_possible_texts = open("./possible_texts.txt", "w")
    index = 0
    length = len(content) - 2
    while index < length: 
        substring = content[index : index + 3]
        substring_key = substring[0] + substring[1] + substring[2]
        if substring_key not in appearance_indexes:
            appearance_indexes[substring_key] = [index]
        else:
            appearance_indexes[substring_key].append(index)
        index += 1
    temp_dict = dict(appearance_indexes)
    for key, value in temp_dict.items():
        if len(value) == 1:
            del appearance_indexes[key]
    divisors_frequency = dict()
    for key, value in appearance_indexes.items():
        divisors = []
        value_length = len(value)
        for i in range(1, value_length - 1):
            divisors = divisors + get_divisors(value[i] - value[i - 1])
        for divisor in divisors:
            if divisor in divisors_frequency:
                divisors_frequency[divisor] += 1
            else:
                divisors_frequency[divisor] = 1
    divisors_frequency = {k: v for k, v in sorted(divisors_frequency.items(), key=lambda item: item[1], reverse=True)}
    temp_dict = dict(divisors_frequency)
    for key, value in temp_dict.items():
        if key >= 20:
            del divisors_frequency[key]
    for key, value in divisors_frequency.items():
        wrong_length = False
        subkeys = []
        i = 0
        while i < key:
            row = ""
            j = i
            while j < len(content) - 1:
                row = row + content[j]
                j += key
            i += 1
            subkey = find_subkey(row)
            if subkey == 0:
                wrong_length = True
                break
            else:
                subkeys.append(subkey)
        if wrong_length:
            continue
        else:
            combinations = []
            for element in itertools.product(*subkeys):
                combinations.append(element)
            for combination in combinations:
                key = ""
                for subkey in combination:
                    key = key + subkey
                decrypted_text = decrypt(path, key)
                if (" " + check_word + " ") in decrypted_text:
                    out_file.write("[KEY]: " + key + "\n[TEXT]:" + decrypt(path, key) + "\n")
                if " AND " in decrypted_text or " THE " in decrypted_text: 
                    all_possible_texts.write("[KEY]: " + key + "\n[TEXT]:" + decrypt(path, key) + "\n")

def get_divisors(number):
    res = []
    for i in range(2, number):
        if number % i == 0:
            res.append(i)
    return res

def find_subkey(row):
    subkey_match = dict()
    for key, value in sym_num_alphabet.items(): 
        decrypted_row = ""
        for symbol in row:
            if symbol in sym_num_alphabet:
                symbol_number = sym_num_alphabet[symbol]
                shift = sym_num_alphabet[key]
                decrypted_row = decrypted_row + num_sym_alphabet[(symbol_number - shift) % ALPHABET_SIZE]
            else:
                decrypted_row = decrypted_row + symbol
        subkey_match[key] = find_frequency_match(decrypted_row)
    max_value = max(subkey_match.values())
    if max_value == 0:
        return 0
    else:
        max_keys = [k for k, v in subkey_match.items() if v == max_value]
        return max_keys

def find_frequency_match(row):
    frequency = dict()
    result_score = 0
    for letter in row:
        if letter in frequency:
            frequency[letter] += 1
        else:
            frequency[letter] = 1
    for key, value in frequency.items():
        if abs(value / len(row) - letter_frequency_distribution[key]) <= 0.05:
            result_score += 1
    return result_score 

def decrypt(path, key):
    file = open(path, "r")
    content = file.read()
    out_text = ""
    key_symbol_counter = 0
    for symbol in content:
        if symbol in sym_num_alphabet:
            symbol_number = sym_num_alphabet[symbol]
            shift = sym_num_alphabet[key[key_symbol_counter]]
            out_text = out_text + num_sym_alphabet[(symbol_number - shift) % ALPHABET_SIZE]
            if key_symbol_counter == len(key) - 1:
                key_symbol_counter = 0
            else:
                key_symbol_counter += 1 
        else:
            out_text = out_text + symbol
    file.close()
    return out_text

babbage_attack("./upper_vigenere.txt", "LANGUAGES")