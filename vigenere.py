from string import ascii_lowercase, ascii_uppercase

ALPHABET = ascii_uppercase + ascii_lowercase
ALPHABET_SIZE = len(ALPHABET)

sym_num_alphabet={}
num_sym_alphabet={}

for idx in range(ALPHABET_SIZE):
    key = ALPHABET[idx]
    sym_num_alphabet[key] = idx
    num_sym_alphabet[idx] = key

def encrypt(path, key):
    file = open(path, "r")
    content = file.read()
    out_file = open("./encrypt.txt", "w")
    key_symbol_counter = 0
    for symbol in content:
        if symbol in sym_num_alphabet:
            symbol_number = sym_num_alphabet[symbol]
            shift = sym_num_alphabet[key[key_symbol_counter]]
            out_file.write(num_sym_alphabet[(symbol_number + shift) % ALPHABET_SIZE])
            if key_symbol_counter == len(key) - 1:
                key_symbol_counter = 0
            else:
                key_symbol_counter += 1
        else:
            out_file.write(symbol)
    out_file.close()
    file.close()


def decrypt(path, key):
    file = open(path, "r")
    content = file.read()
    out_file = open("./decrypt.txt", "w")
    key_symbol_counter = 0
    for symbol in content:
        if symbol in sym_num_alphabet:
            symbol_number = sym_num_alphabet[symbol]
            shift = sym_num_alphabet[key[key_symbol_counter]]
            out_file.write(num_sym_alphabet[(symbol_number - shift) % ALPHABET_SIZE])
            if key_symbol_counter == len(key) - 1:
                key_symbol_counter = 0
            else:
                key_symbol_counter += 1
        else:
            out_file.write(symbol)
    out_file.close()
    file.close()


encrypt("./var5.txt", "DIAKONOV")
decrypt("./encrypt.txt", "DIAKONOV")
