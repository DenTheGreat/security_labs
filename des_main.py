from des import DesKey

single_key = DesKey(u'Д’яконов'.encode('cp1251'))
triple_key = DesKey(u'Д’яконовДенисКир'.encode('cp1251'))

with open('./var5.txt','rb') as file:
    src = file.read()

    print('DES')
    print(single_key.encrypt(src, padding=True))

    print('Triple DES')
    print(triple_key.encrypt(src, padding=True))